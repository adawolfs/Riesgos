import { Mongo } from 'meteor/mongo';

export const Accion = new Mongo.Collection('accion');

Accion.schema = new SimpleSchema({
	planM : {type: String},
	planC : {type: String},
	deliveries : {type: String},
	activity : {type: String},
	dependency : {type: String},
	time : {type: String},
	deadLine : {type: String},
	cost : {type: String},
	cause: {type: String, regEx: SimpleSchema.RegEx.Id, optional: false},
})

//Accion.attachSchema(Accion.schema);
