import { Mongo } from 'meteor/mongo';

export const Cause = new Mongo.Collection('cause');

Cause.schema = new SimpleSchema({
	description : {type: String},
	consequence : {type: String},
	trigger : {type: String},
	risk: {type: String, regEx: SimpleSchema.RegEx.Id, optional: false}
})

//Cause.attachSchema(Cause.schema);
