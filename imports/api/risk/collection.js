import { Mongo } from 'meteor/mongo';

export const Risk = new Mongo.Collection('risk');

Risk.schema = new SimpleSchema({
	name : {type: String},
	description : {type: String},
	project: {type: String, regEx: SimpleSchema.RegEx.Id, optional: false}
})


