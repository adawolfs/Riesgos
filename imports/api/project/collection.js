import { Mongo } from 'meteor/mongo';

export const Project = new Mongo.Collection('project');

Project.schema = new SimpleSchema({
	name : {type: String},
	description : {type: String}
})

//Project.attachSchema(Project.schema);
