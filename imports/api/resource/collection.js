import { Mongo } from 'meteor/mongo';

export const Resource = new Mongo.Collection('resource');

Resource.schema = new SimpleSchema({
	description : {type: String},
	cost : {type: String},
	accion: {type: String, regEx: SimpleSchema.RegEx.Id, optional: false},
})

//Resource.attachSchema(Resource.schema);
