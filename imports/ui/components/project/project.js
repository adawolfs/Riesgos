import angular from 'angular';
import angularMeteor from 'angular-meteor';

import './project.html';
import { name as ProjectList } from '../projectList/projectList';
import { modal } from 'angular-ui-bootstrap/src/modal';
import ngMaterial from 'angular-material';

class Project {}

const name = 'project';

// create a module
export default angular.module(name, [
  angularMeteor,
  ProjectList,
  ngMaterial
]).component(name, {
  templateUrl: `imports/ui/components/${name}/${name}.html`,
  controllerAs: name,
  controller: Project
});