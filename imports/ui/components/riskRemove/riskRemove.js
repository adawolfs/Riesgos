import angular from 'angular';
import angularMeteor from 'angular-meteor';
 
import './riskRemove.html';
import { Risk } from '../../../api/risk/collection';

class RiskRemove {
  constructor(){
     ("rock")
  }
  remove() {
     (this.risk)
    if (this.risk) {
       (this.risk)
      Risk.remove(this.risk._id);
    }
  }
}
 
const name = 'riskRemove';
 
// create a module
export default angular.module(name, [
  angularMeteor
]).component(name, {
  templateUrl: `imports/ui/components/${name}/${name}.html`,
  bindings: {
    risk: '='
  },
  controllerAs: name,
  controller: RiskRemove
});