import angular from 'angular';
import angularMeteor from 'angular-meteor';
 
import './accionAdd.html';
import { Accion } from '../../../api/accion/collection';

class AccionAdd {
  constructor($scope, $reactive){
    console.log(this.cause)
		'ngInject';
		$reactive(this).attach($scope);
    this.accion = {};
    this.cause = this.getReactively('cause', true);
  }
 
  submit() {
    this.accion.cause = this.cause._id
    Accion.insert(this.accion);
    this.reset();
  }

  reset() {
  	 ("reset inside")
  	this.accion = {};
  }
}
 
const name = 'accionAdd';
 
// create a module
export default angular.module(name, [
  angularMeteor
]).component(name, {
  templateUrl: `imports/ui/components/${name}/${name}.html`,
   bindings: {
    cause: '='
  },
  controllerAs: name,
  controller: AccionAdd
});