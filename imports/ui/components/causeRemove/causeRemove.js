import angular from 'angular';
import angularMeteor from 'angular-meteor';
 
import './causeRemove.html';
import { Cause } from '../../../api/cause/collection';

class CauseRemove {
  constructor(){
     (this.cause)
  }
  remove() {
     (this.cause)
    if (this.cause) {
      Cause.remove(this.cause._id);
    }
  }
}
 
const name = 'causeRemove';
 
// create a module
export default angular.module(name, [
  angularMeteor
]).component(name, {
  templateUrl: `imports/ui/components/${name}/${name}.html`,
  bindings: {
    cause: '<'
  },
  controllerAs: name,
  controller: CauseRemove
});