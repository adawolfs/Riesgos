import angular from 'angular';
import angularMeteor from 'angular-meteor';
 
import './projectAdd.html';
import { Project } from '../../../api/project/collection';

class ProjectAdd {
  constructor() {
    this.project = {};
  }
 
  submit() {
     ("insert")
    Project.insert(this.project);
     ("reset")
    this.reset();
     ("finish")

  }

  reset() {
  	 ("reset inside")
  	this.project = {};
  }
}
 
const name = 'projectAdd';
 
// create a module
export default angular.module(name, [
  angularMeteor
]).component(name, {
  templateUrl: `imports/ui/components/${name}/${name}.html`,
  controllerAs: name,
  controller: ProjectAdd
});