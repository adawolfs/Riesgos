import angular from 'angular';
import angularMeteor from 'angular-meteor';
 
import './causeEdit.html';
import { Cause } from '../../../api/cause/collection';

class CauseEdit {
  constructor($scope, $reactive){
		'ngInject';
		$reactive(this).attach($scope);
    	this.cEdition = {};
      this.cEdition.description = this.cause.description
      this.cEdition.consequence = this.cause.consequence
      this.cEdition.trigger = this.cause.trigger
      console.log(this.cEdition)
  }
 
  submit() {
    console.log(this.cause)
    console.log(this.cEdition)
    Cause.update({_id: this.cause._id}, 
      {$set: 
        {
          description: this.cEdition.description, 
          consequence: this.cEdition.consequence,
          trigger: this.cEdition.trigger
        }
      })
  }

}
 
const name = 'causeEdit';
 
// create a module
export default angular.module(name, [
  angularMeteor
]).component(name, {
  templateUrl: `imports/ui/components/${name}/${name}.html`,
   bindings: {
    cause: '='
  },
  controllerAs: name,
  controller: CauseEdit
});