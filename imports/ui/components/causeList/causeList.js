import angular from 'angular';
import angularMeteor from 'angular-meteor';

import './causeList.html'
import { Cause } from '../../../api/cause/collection';
import { name as CauseAdd } from '../causeAdd/causeAdd';
import { name as CauseEdit } from '../causeEdit/causeEdit';
import { name as CauseRemove } from '../causeRemove/causeRemove';
import { name as AccionList } from '../accionList/accionList';

class CauseList {
	constructor($scope, $reactive){
		'ngInject';
		$reactive(this).attach($scope);
		this.helpers({
			causes(){
				const risk = this.getReactively('risk', true);
				this.risk = risk
				return Cause.find({risk: risk._id});
			}
		})
	}
}

const name = 'causeList';
export default angular.module(name,[
	angularMeteor,
	CauseRemove,
	AccionList,
	CauseAdd,
	CauseEdit
]).component(name, {
  templateUrl: `imports/ui/components/${name}/${name}.html`,
  bindings: {
    risk: '='
  },
  controllerAs: name,
  controller: CauseList
});