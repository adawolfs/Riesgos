import angular from 'angular';
import angularMeteor from 'angular-meteor';
 
import './projectRemove.html';
import { Project } from '../../../api/project/collection';

class ProjectRemove {
  remove() {
    if (this.project) {
      Project.remove(this.project._id);
    }
  }
}
 
const name = 'projectRemove';
 
// create a module
export default angular.module(name, [
  angularMeteor
]).component(name, {
  templateUrl: `imports/ui/components/${name}/${name}.html`,
  bindings: {
    project: '<'
  },
  controllerAs: name,
  controller: ProjectRemove
});