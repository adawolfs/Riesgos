import angular from 'angular';
import angularMeteor from 'angular-meteor';

import './resourceList.html'
import { Resource } from '../../../api/resource/collection';
import { name as ResourceAdd } from '../resourceAdd/resourceAdd';
import { name as ResourceEdit } from '../resourceEdit/resourceEdit';
import { name as ResourceRemove } from '../resourceRemove/resourceRemove';	
class ResourceList {
	constructor($scope, $reactive){
		'ngInject';
		$reactive(this).attach($scope);
		this.helpers({
			resources(){
				const accion = this.getReactively('accion', true);
				this.accion = accion
				console.log(accion)
				return Resource.find({accion: accion._id});
			}
		})
	}
}

const name = 'resourceList';
export default angular.module(name,[
	angularMeteor,
	ResourceAdd,
	ResourceEdit,
	ResourceRemove
]).component(name, {
  templateUrl: `imports/ui/components/${name}/${name}.html`,
  bindings: {
    accion: '='
  },
  controllerAs: name,
  controller: ResourceList
});