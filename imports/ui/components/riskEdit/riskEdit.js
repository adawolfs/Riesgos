import angular from 'angular';
import angularMeteor from 'angular-meteor';
 
import './riskEdit.html';
import { Risk } from '../../../api/risk/collection';

class RiskEdit {
  constructor($scope, $reactive){
		'ngInject';
		$reactive(this).attach($scope);
		this.rEdition = {}
    	this.rEdition.name = this.risk.name;
    	this.rEdition.description = this.risk.description;
  }
 
  submit() {
    Risk.update({_id: this.risk._id}, {$set: {name: this.rEdition.name, description: this.rEdition.description}})
  }

}
 
const name = 'riskEdit';
 
// create a module
export default angular.module(name, [
  angularMeteor
]).component(name, {
  templateUrl: `imports/ui/components/${name}/${name}.html`,
   bindings: {
    risk: '='
  },
  controllerAs: name,
  controller: RiskEdit
});