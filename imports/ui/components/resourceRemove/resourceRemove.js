import angular from 'angular';
import angularMeteor from 'angular-meteor';
 
import './resourceRemove.html';
import { Resource } from '../../../api/resource/collection';

class ResourceRemove {
  remove() {
     (this.resource)
    if (this.resource) {
      Resource.remove(this.resource._id);
    }
  }
}
 
const name = 'resourceRemove';
 
// create a module
export default angular.module(name, [
  angularMeteor
]).component(name, {
  templateUrl: `imports/ui/components/${name}/${name}.html`,
  bindings: {
    resource: '<'
  },
  controllerAs: name,
  controller: ResourceRemove
});