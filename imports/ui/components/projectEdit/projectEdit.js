import angular from 'angular';
import angularMeteor from 'angular-meteor';
 
import './projectEdit.html';
import { Project } from '../../../api/project/collection';

class ProjectEdit {
  constructor() {
    this.pEdition = {}
    this.pEdition.name = this.project.name;
    this.pEdition.description = this.project.description;
  }
 
  submit() {
    Project.update({_id: this.project._id}, {$set: {name: this.pEdition.name, description: this.pEdition.description}})
  }
}
 
const name = 'projectEdit';
 
// create a module
export default angular.module(name, [
  angularMeteor
]).component(name, {
  templateUrl: `imports/ui/components/${name}/${name}.html`,
  bindings: {
    project: '='
  },
  controllerAs: name,
  controller: ProjectEdit
});