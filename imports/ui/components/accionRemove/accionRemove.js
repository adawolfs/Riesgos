import angular from 'angular';
import angularMeteor from 'angular-meteor';
 
import './accionRemove.html';
import { Accion } from '../../../api/accion/collection';

class AccionRemove {
  constructor(){
     console.log(this.cause)
  }
  remove() {
     (this.accion)
    if (this.accion) {
      Accion.remove(this.accion._id);
    }
  }
}
 
const name = 'accionRemove';
 
// create a module
export default angular.module(name, [
  angularMeteor
]).component(name, {
  templateUrl: `imports/ui/components/${name}/${name}.html`,
  bindings: {
    accion: '<',
    cause: '=',
  },
  controllerAs: name,
  controller: AccionRemove
});