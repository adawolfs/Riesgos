import angular from 'angular';
import angularMeteor from 'angular-meteor';
 
import './accionEdit.html';
import { Accion } from '../../../api/accion/collection';

class AccionEdit {
  constructor($scope, $reactive){
		'ngInject';
		$reactive(this).attach($scope);
    this.aEdition = {};
    this.aEdition.planM		 = this.accion.planM		
		this.aEdition.planC		 = this.accion.planC		
		this.aEdition.deliveries = this.accion.deliveries
		this.aEdition.activity	 = this.accion.activity	
		this.aEdition.dependency = this.accion.dependency
		this.aEdition.time		 = this.accion.time		
		this.aEdition.deadLine	 = this.accion.deadLine	
		this.aEdition.cost 		 = this.accion.cost 		
  }
 
  submit() {
  	Accion.update({_id: this.accion._id}, 
  		{$set: 
  			{
  				planM: this.aEdition.planM, 
  				planC: this.aEdition.planC,
  				deliveries: this.aEdition.deliveries,
  				activity: this.aEdition.activity,
  				dependency: this.aEdition.dependency,
  				time: this.aEdition.time,
  				deadLine: this.aEdition.deadLine,
  				cost: this.aEdition.cost
  			}
  		})
  }
}
 
const name = 'accionEdit';
 
// create a module
export default angular.module(name, [
  angularMeteor
]).component(name, {
  templateUrl: `imports/ui/components/${name}/${name}.html`,
   bindings: {
    accion: '='
  },
  controllerAs: name,
  controller: AccionEdit
});