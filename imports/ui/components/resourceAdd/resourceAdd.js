import angular from 'angular';
import angularMeteor from 'angular-meteor';
 
import './resourceAdd.html';
import { Resource } from '../../../api/resource/collection';

class ResourceAdd {
  constructor($scope, $reactive){
		'ngInject';
		$reactive(this).attach($scope);
    this.resource = {};
		this.accion = this.getReactively('accion', true);
  }
 
  submit() {
    this.resource.accion = this.accion._id
    Resource.insert(this.resource);
    this.reset();
  }

  reset() {
  	 ("reset inside")
  	this.resource = {};
  }
}
 
const name = 'resourceAdd';
 
// create a module
export default angular.module(name, [
  angularMeteor
]).component(name, {
  templateUrl: `imports/ui/components/${name}/${name}.html`,
   bindings: {
    accion: '='
  },
  controllerAs: name,
  controller: ResourceAdd
});