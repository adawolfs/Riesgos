import angular from 'angular';
import angularMeteor from 'angular-meteor';

import './projectList.html'
import { Project } from '../../../api/project/collection';
import { name as ProjectAdd } from '../projectAdd/projectAdd';
import { name as ProjectEdit } from '../projectEdit/projectEdit';
import { name as ProjectRemove} from '../projectRemove/projectRemove'
import { name as RiskList} from '../riskList/riskList'

class ProjectList {
	constructor($scope, $reactive){
		'ngInject';
		$reactive(this).attach($scope);
		this.helpers({
			projects(){
				return Project.find({});
			}
		})
	}
}

const name = 'projectList';

export default angular.module(name,[
	angularMeteor,
	ProjectAdd,
	ProjectRemove,
	RiskList,
	ProjectEdit
]).component(name, {
  templateUrl: `imports/ui/components/${name}/${name}.html`,
  controllerAs: name,
  controller: ProjectList
});