import angular from 'angular';
import angularMeteor from 'angular-meteor';
 
import './causeAdd.html';
import { Cause } from '../../../api/cause/collection';

class CauseAdd {
  constructor($scope, $reactive){
    console.log(this.risk)
		'ngInject';
		$reactive(this).attach($scope);
    this.cause = {};
		this.risk = this.getReactively('risk', true);
    console.log(this.risk)
  }
 
  submit() {
    this.cause.risk = this.risk._id
    Cause.insert(this.cause);
    this.reset();
  }

  reset() {
  	 ("reset inside")
  	this.cause = {};
  }
}
 
const name = 'causeAdd';
 
// create a module
export default angular.module(name, [
  angularMeteor
]).component(name, {
  templateUrl: `imports/ui/components/${name}/${name}.html`,
   bindings: {
    risk: '='
  },
  controllerAs: name,
  controller: CauseAdd
});