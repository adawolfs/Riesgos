import angular from 'angular';
import angularMeteor from 'angular-meteor';
 
import './resourceEdit.html';
import { Resource } from '../../../api/resource/collection';

class ResourceEdit {
  constructor($scope, $reactive){
		'ngInject';
		$reactive(this).attach($scope);
    	this.rEdition = {};
		this.rEdition.description = this.resource.description;
		this.rEdition.cost = this.resource.cost;
  }
 
  submit() {
    Resource.update({_id: this.resource._id}, 
      {$set: 
        {
          description: this.rEdition.description, 
          cost: this.rEdition.cost
        }
      })
  }
}
 
const name = 'resourceEdit';
 
// create a module
export default angular.module(name, [
  angularMeteor
]).component(name, {
  templateUrl: `imports/ui/components/${name}/${name}.html`,
   bindings: {
    resource: '='
  },
  controllerAs: name,
  controller: ResourceEdit
});