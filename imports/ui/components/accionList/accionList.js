import angular from 'angular';
import angularMeteor from 'angular-meteor';

import './accionList.html'
import { Accion } from '../../../api/accion/collection';
import { name as AccionAdd } from '../accionAdd/accionAdd';
import { name as AccionRemove } from '../accionRemove/accionRemove';
import { name as AccionEdit } from '../accionEdit/accionEdit';
import { name as ResourceList } from '../resourceList/resourceList';

class AccionList {
	constructor($scope, $reactive){
		'ngInject';
		$reactive(this).attach($scope);
		this.helpers({
			accions(){
				const cause = this.getReactively('cause', true);
				this.cause = cause
				return Accion.find({cause: cause._id});
			}
		})
	}
}

const name = 'accionList';
export default angular.module(name,[
	angularMeteor,
	AccionAdd,
	AccionRemove,
	AccionEdit,
	ResourceList
]).component(name, {
  templateUrl: `imports/ui/components/${name}/${name}.html`,
  bindings: {
    cause: '='
  },
  controllerAs: name,
  controller: AccionList
});