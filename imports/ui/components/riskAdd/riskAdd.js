import angular from 'angular';
import angularMeteor from 'angular-meteor';
 
import './riskAdd.html';
import { Risk } from '../../../api/risk/collection';
import { Project } from '../../../api/project/collection';

class RiskAdd {
  constructor($scope, $reactive){
		'ngInject';
		$reactive(this).attach($scope);
    	this.risk = {};
      this.project = this.getReactively('project', true);
  }
 
  submit() {
    this.risk.project = this.project._id
    Risk.insert(this.risk);
    this.reset();
  }

  reset() {
  	 ("reset inside")
  	this.risk = {};
  }
}
 
const name = 'riskAdd';
 
// create a module
export default angular.module(name, [
  angularMeteor
]).component(name, {
  templateUrl: `imports/ui/components/${name}/${name}.html`,
   bindings: {
    project: '='
  },
  controllerAs: name,
  controller: RiskAdd
});