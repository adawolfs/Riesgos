import angular from 'angular';
import angularMeteor from 'angular-meteor';

import './riskList.html'
import { Risk } from '../../../api/risk/collection';
import { name as RiskAdd } from '../riskAdd/riskAdd';
import { name as RiskRemove } from '../riskRemove/riskRemove';
import { name as RiskEdit } from '../riskEdit/riskEdit';
import { name as CauseList } from '../causeList/causeList';

class RiskList {
	constructor($scope, $reactive){
		'ngInject';
		$reactive(this).attach($scope);
		this.helpers({
			risks(){
				const project = this.getReactively('project', true);
				this.project = project
				return Risk.find({project: project._id});
			}
		})
	}	
}

const name = 'riskList';
export default angular.module(name,[
	angularMeteor,
	RiskRemove,
	RiskEdit,
	CauseList,
	RiskAdd
]).component(name, {
  templateUrl: `imports/ui/components/${name}/${name}.html`,
  bindings: {
    project: '='
  },
  controllerAs: name,
  controller: RiskList
});